<?php
require_once('Database.php');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$db = new Database("127.0.0.1", "coffee_list", "root", '');

if ($_SERVER['REQUEST_METHOD'] == "GET"){
    // on get request gets all the data and converts it to json
    echo json_encode($db->query("SELECT * FROM coffee"));
    http_response_code(200);

} else if ($_SERVER['REQUEST_METHOD'] == "POST"){

    $data = json_decode(file_get_contents("php://input"));
    if(isset($data->delete)){

        $id = $data->id;
        $query = "DELETE FROM coffee WHERE id='" . $id . "'";

        $db->query($query);
        http_response_code(200);

    } else {
        $title = $data->title;
        $price = $data->price;
        $image_url = $data->image_url;

        $query = "INSERT INTO coffee (title, price, image_url)
        VALUES ('" . $title . "','" . $price . "' ,' " . $image_url . "')";

        $db->query($query);
        http_response_code(200);
    }

}
