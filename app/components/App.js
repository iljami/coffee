var _ = require('lodash');
var axios = require('axios');
var React = require('react');
var Modal = require('./Modal');
var Coffee = require('./Coffee');

class App extends React.Component {
	constructor(props){
		super(props);

		this.state = {
			coffee_items: []
		};
	}

	componentDidMount(){
		axios.get('http://localhost/coffee/api/api.php').then(res => {
      		//const items = res.data;
      		this.setState({coffee_items: res.data});
      		//console.log(res.data)
      	});

	}

	updateElements(){
   		
	   	axios.get('http://localhost/coffee/api/api.php').then(res => {
		    this.setState({coffee_items: res.data});
      	});
   		
    }

    insertCoffee(input_title, input_price, input_image_url){

        axios.post('http://localhost/coffee/api/api.php', {
			title: input_title,
			price: input_price,
			image_url: input_image_url
		});

		this.updateElements();
	}	

	deleteCoffee(input_id){
		// console.log(input_id);
		axios.post('http://localhost/coffee/api/api.php', {
			delete: 'delete',
			id: input_id
		});
		
		this.updateElements();
	}

	render() {
		return (
			<div>
				<Modal insertCoffee={this.insertCoffee.bind(this)} updateElements={this.updateElements.bind(this)} deleteCoffee={this.deleteCoffee.bind(this)} />
				<Coffee coffee_items={this.state.coffee_items} updateElements={this.updateElements.bind(this)} deleteCoffee={this.deleteCoffee.bind(this)}/>
			</div>
    	);
   }
}
module.exports = App;