var React = require('react');
var CoffeeItem = require('./CoffeeItem');
var axios = require('axios');

class Coffee extends React.Component {
	renderItems(){
		return _.map(this.props.coffee_items, (coffee_item, index) => <CoffeeItem key={index} {...coffee_item} updateElements={this.props.updateElements} deleteCoffee={this.props.deleteCoffee} />);
	}

	render(){
		return (
			<div className="row">
				{this.renderItems()}
			</div>
		);

		 
	}
}
module.exports = Coffee;