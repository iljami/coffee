var React = require('react');
var axios = require('axios');

class CoffeeItem extends React.Component{
	render(){
		return (
			<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
				<div className="item-container">
					<div className="remove-button">
						<button type="button" onClick={this.handleDelete.bind(this)} className="btn btn-danger btn-lg"
						aria-label="Close">&times;
						</button>
					</div>
					<div className="image-container">
                        <img className="img-responsive" src={this.props.image_url} />
                    </div>
                    <div className="item-title">
                        <p>{this.props.title}</p><strong>{this.props.price}</strong>
                    </div>
				</div>
			</div>
		);

	}
	handleDelete(){
		this.props.deleteCoffee(this.props.id);
		this.props.updateElements();
	}

}
module.exports = CoffeeItem;