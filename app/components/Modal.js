var React = require('react');
var axios = require('axios');

class Modal extends React.Component {

	// insertCoffee(input_title, input_price, input_image_url){

	// 	axios.post('http://localhost/coffee/api/api.php', {
	// 		title: input_title,
	// 		price: input_price,
	// 		image_url: input_image_url
	// 	}).then(function (response){
	// 		console.log(response);
	// 	}).catch(function (error){
	// 		console.log(error);
	// 	});
	// }	

	render(){
		return (
			<div>
				<div className="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel">
	                <div className="modal-dialog" role="document">
	                    <div className="modal-content">
	                        <div className="modal-header">
	                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                            <h4 className="modal-title" id="exampleModalLabel">Add a coffee item</h4>
	                        </div>
	                        <form onSubmit={this.handleAdd.bind(this)}>
	                            <div className="modal-body">
	                                <div className="form-group">
	                                    <label className="control-label">Title:</label>
	                                    <input type="text" className="form-control" id="recipient-name" ref="title" />
	                                </div>
	                                <div className="form-group">
	                                    <label className="control-label">Price:</label>
	                                    <input type="text" className="form-control" id="recipient-name" ref="price" />
	                                </div>
	                                <div className="form-group">
	                                    <label className="control-label">Image URL:</label>
	                                    <input type="text" className="form-control" id="recipient-name" ref="image_url" />
	                                </div>
	                            </div>
	                            <div className="modal-footer">
	                                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
	                                <button type="submit" onClick={this.props.updateElements} className="btn btn-primary">Add an item</button>
	                            </div>
	                        </form>
	                    </div>
	                </div>
	            </div>
	            <div className="add-button">
	                <button type="button" className="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal">+ Add</button>
	            </div>
            </div>
		);
	}

	handleAdd(event){
		event.preventDefault();

		const title = this.refs.title.value;
		const price = this.refs.price.value;
		const image_url = this.refs.image_url.value;

		this.props.insertCoffee(title, price, image_url);
		this.props.updateElements();

		this.refs.title.value = '';
		this.refs.price.value = '';
		this.refs.image_url.value = '';

	}

	
}
module.exports = Modal;