var React = require('react');

class Test extends React.Component {
	render(){
		const coffee_items = [
			{
				title: 'Espresso',
				price: '1.2 EUR',
				image_url: 'https://dummyimage.com/200'
			},
			{
				title: 'Cappucino',
				price: '2.0 EUR',
				image_url: 'https://dummyimage.com/300'
			}
		];

		const coffees = coffee_items.map(coffee => {
			return (
				<h1>{coffee.title}</h1>
			)
		});

		return <div>{coffees}</div>;
	}
}
module.exports = Test;