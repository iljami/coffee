<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Coffee list</title>

    <!-- Bootstrap -->
    <link href="/coffee/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/coffee/assets/css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <div class="container">
        <h1 class="page-header">Coffee list application</h1>
        <div class="wrapper">
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Add a coffee item</h4>
                        </div>
                        <form action="api/api.php" method="POST">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Title:</label>
                                    <input type="text" class="form-control" id="recipient-name" name="title">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Price:</label>
                                    <input type="text" class="form-control" id="recipient-name" name="price">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Image URL:</label>
                                    <input type="text" class="form-control" id="recipient-name" name="image_url">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Add an item</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> <!-- modal -->
            <div class="add-button">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal">+ Add</button>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="item-container">
                        <div class="remove-button">
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <img class="img-responsive" src="https://dummyimage.com/200">
                        <div class="item-title">
                            <p>Espresso</p><strong>1,2EUR</strong>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="item-container">
                        <div class="remove-button">
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <img class="img-responsive" src="https://dummyimage.com/200">
                        <div class="item-title">
                            <p>Espresso</p><strong>1,2EUR</strong>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="item-container">
                        <div class="remove-button">
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <img class="img-responsive" src="https://dummyimage.com/200">
                        <div class="item-title">
                            <p>Espresso</p><strong>1,2EUR</strong>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="item-container">
                        <div class="remove-button">
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <img class="img-responsive" src="https://dummyimage.com/200">
                        <div class="item-title">
                            <p>Espresso</p><strong>1,2EUR</strong>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div class="item-container">
                        <div class="remove-button">
                            <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal" aria-label="Close">&times;</button>
                        </div>
                        <img class="img-responsive" src="https://dummyimage.com/200">
                        <div class="item-title">
                            <p>Espresso </p><strong>1,2EUR</strong>
                        </div>
                    </div>
                </div>
                <div class="testing">
                    <form action="api/api.php" method="get">
                        <button type="submit">GET</button>
                    </form>
                </div>
                <div class="testing">
                    <button id="del" type="button">DELETE</button>
                </div>
                <div class="testing">
                    <form action="api/api.php" method="post">
                        <button type="submit">POST</button>
                    </form>
                </div>
                <div id="result">

                </div>
            </div>
        </div> <!--wrapper-->
    </div> <!-- container-->

<script src="/coffee/assets/js/jquery-3.2.1.min.js"></script>
<script src="/coffee/assets/js/bootstrap.min.js"></script>
</body>
</html>